<?php

  include("config/dbconnection.php");
  include("config/settings.php");

  $query = "SELECT * FROM courses;";

  if ($result = mysqli_query($link, $query, MYSQLI_USE_RESULT)) {
         // Printing courses registers in website
         print(INDEX_COURSES_TITLE);
         print("<table>");
         printf("<tr><th>%s</th> <th>%s</th> <th>%s</th> <th>%s</th> <th></th> <th></th></tr>", 
                 COURSE, SCHEDULE, START_DATE, END_DATE);
         while ($row = mysqli_fetch_row($result)) {
            printf("<tr><td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td>
            <td><a href=\"courses/delete.php?id=%u\">%s</a></td>
            <td><a href=\"courses.php?action=edit&id=%u\">%s</a></td> </tr>", $row[0],
            $row[1], $row[2], $row[3], $row[4], DELETE, $row[4], EDIT);
         }
         print("</table>");
         printf("<p><center><a href=\"courses.php?action=new\">%s</a> <a href=\"courses.php\">%s</a></center></p>", ADD, BACK);
         mysqli_free_result($result);
  }

  // Closing conecction
  mysqli_close($link);

?>
