<?php

  include("../config/dbconnection.php");

  // Getting post values
  $name = $_POST["name"];
  $lastname1 = $_POST["lastname1"];
  $lastname2 = $_POST["lastname2"];
  $email = $_POST["email"];
  $category = "student";
  $course_id = $_POST["course_id"];

  // Inserting course in database
  $query = "INSERT INTO users (id, name, lastname1, lastname2, email, category) 
            VALUES ('0', '$name', '$lastname1', '$lastname2', '$email', '$category'); ";
  mysqli_query($link, $query);

  // Consulting id from user
  $query2 = "SELECT id FROM users ORDER BY id DESC LIMIT 1";
  $result = mysqli_query($link, $query2, MYSQLI_USE_RESULT);
  $row = mysqli_fetch_row($result);
  $user_id = $row[0]; 
  mysqli_free_result($result);

  // Creating subscription in database
  $query3 = "INSERT INTO subscriptions (id, course_id, user_id) VALUES ('0', '$course_id', '$user_id');";
  mysqli_query($link, $query3);
  
  // Closing connection
  mysqli_close($link);

  // Redirecting to index
  header('Location: ../sign_in.php?action=success ');

?>
