<?php

 // Checking id is really a number
  $id = intval($_GET["id"]);

  include("config/dbconnection.php");
  include("config/settings.php");


  $query = "SELECT * FROM courses WHERE id = '$id';";

  if ($result = mysqli_query($link, $query, MYSQLI_USE_RESULT)) {
         // Printing course register in website
         print(SHOW_COURSES_TITLE);
         print("<table>");
         printf("<tr><th>%s</th> <th>%s</th> <th>%s</th> <th>%s</th></tr>", COURSE, SCHEDULE, START_DATE, END_DATE);
         $row = mysqli_fetch_row($result);
         printf("<tr><td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td></tr>", $row[0], $row[1], $row[2], $row[3]);
         print("</table>");
         mysqli_free_result($result);
  }


  // Getting students subscribed in course
  $query2 = "SELECT * FROM subscriptions WHERE course_id = '$id';";

  if ($result1 = mysqli_query($link, $query2, MYSQLI_USE_RESULT)) {
        $users = array();
        while ($row = mysqli_fetch_row($result1)) {
          array_push($users, $row[2]);
        }    
        mysqli_free_result($result1);
  }

  print(SHOW_STUDENTS_TITLE);
  print("<table>");
  printf("<tr><th>%s</th><th>%s</th></tr>", STUDENT, EMAIL);

  // Printing students subscribed in course
  foreach( $users as $user){
    $query3 = "SELECT * FROM users WHERE id = '$user'";

    if ($result = mysqli_query($link, $query3, MYSQLI_USE_RESULT)) {
         // Printing students registers in website       
         while ($row = mysqli_fetch_row($result)) {
            printf("<tr><td>%s %s %s</td> <td>%s</td></tr>", $row[1], $row[2], $row[3], $row[4]);
         }         
         mysqli_free_result($result);
    }
  }
  print("</table>");

  printf("<p><center><a href=\"courses.php\">%s</a></center></p>", BACK);

  // Closing conecction
  mysqli_close($link);

?>
