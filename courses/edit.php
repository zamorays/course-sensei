<?php

  // Checking id is really a number
  $id = intval($_GET["id"]);

  include("config/settings.php");
  include("config/dbconnection.php");

  $query = "SELECT * FROM courses WHERE id = '$id';";

  if ($result = mysqli_query($link, $query, MYSQLI_USE_RESULT)) {
   // Printing form with values from database query

   $row = mysqli_fetch_row($result);
   printf("%s", EDIT_COURSES_TEXT);
   print("<p>");
   print("<form method=\"post\" action=\"courses/update.php\">");
   printf("%s <input type=\"text\" name=\"name\" value =\"%s\"><br />", EDIT_COURSE, $row[0]);
   printf("%s <input type=\"text\" name=\"schedule\" value =\"%s\"><br />", EDIT_SCHEDULE, $row[1]);
   printf("%s <input type=\"text\" name=\"start_date\" value =\"%s\"><br />", EDIT_START_DATE, $row[2]);
   printf("%s <input type=\"text\" name=\"end_date\" value =\"%s\"><br />", EDIT_END_DATE, $row[3]);
   printf("<input type=\"hidden\" name=\"id\" value=\"%u\">", $row[4]);
   printf("<input type=\"submit\" value=\"%s\"></form>", EDIT);
   print("</p>");
   printf('<p><a href="courses.php">%s</a></p>', BACK);

   mysqli_free_result($result);
  }

  // Closing connection
  mysqli_close($link);

?>

